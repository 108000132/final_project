// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class create_account extends cc.Component {

    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        let button = new cc.Component.EventHandler();
        button.target = this.node;
        button.component ="create_account";
        button.handler = "create";
        button.customEventData = "";
        cc.find("enter").getComponent(cc.Button).clickEvents.push(button);

        let back = new cc.Component.EventHandler();
        back.target = this.node;
        back.component ="create_account";
        back.handler = "back";
        back.customEventData = "";
        cc.find("back").getComponent(cc.Button).clickEvents.push(back);
    }
    back(){
        cc.director.loadScene("login");
    }
    create(){
        let email = cc.find("email").getComponent(cc.EditBox).string;
        let password = cc.find("password").getComponent(cc.EditBox).string;
        let id = cc.find("id").getComponent(cc.EditBox).string;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(result) {
            email = "";
            password = "";
            var Ref = firebase.database().ref(id+'/record');
            var data = {
                best_money: 0,
                fuck:true,
            };
            Ref.set(data);
            console.log("success");
            cc.director.loadScene("login");
        }).catch(function(error) {
            email = "";
            password = "";
            cc.log("wrong");
        });
    }

    start () {
        
    }

    // update (dt) {}
}
