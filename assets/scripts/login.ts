// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class login extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        let button_enter = new cc.Component.EventHandler();
        button_enter.target = this.node;
        button_enter.component ="login";
        button_enter.handler = "startgame";
        button_enter.customEventData = "";
        cc.find("enter").getComponent(cc.Button).clickEvents.push(button_enter);

        let button_account = new cc.Component.EventHandler();
        button_account.target = this.node;
        button_account.component ="login";
        button_account.handler = "new_account";
        button_account.customEventData = "";
        cc.find("new_account").getComponent(cc.Button).clickEvents.push(button_account);

    }
    new_account(){
        cc.director.loadScene("create_account");
    }
    startgame(){
        let email = cc.find("email").getComponent(cc.EditBox).string;
        let password = cc.find("password").getComponent(cc.EditBox).string;
        let id = cc.find("id").getComponent(cc.EditBox).string;
        firebase.auth().signInWithEmailAndPassword(email,password).then(function(result) {
            var today = new Date();
            var current = {
                year : today.getFullYear(),
                month: today.getMonth()+1,
                day: today.getDate(),
                hour:today.getHours(),
                minute:today.getMinutes()
            };
            var Ref = firebase.database().ref(id+'/last_game_time');
            var data = {
                year : current.year,
                month: current.month,
                day: current.day,
                hour: current.hour,
                minute: current.minute
            };
            Ref.set(data);
            Ref = firebase.database().ref(id+"/record");
            Ref.once('value',(snapshot) =>{
                var record = snapshot.val();
                cc.sys.localStorage.setItem('best_record', record.best_money);
            });
            cc.sys.localStorage.setItem('cur_player', email);
            cc.director.loadScene("menu");
        }).catch(function(error) {
            console.log(error);
        });
    }
    start () {

    }

    // update (dt) {}
}
